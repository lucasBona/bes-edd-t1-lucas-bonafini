package br.ucsal.bes201902.edd.lista04;

import java.util.Scanner;

public class Questao06 {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumeroCalcularQuociente();

	}

	private static void obterNumeroCalcularQuociente() {
		Integer x, y;
		System.out.println("Usu�rio, digite dois n�meros para dividir");
		x = sc.nextInt();
		y = sc.nextInt();
		System.out.println("O quociente da divis�o de " + x + " por " + y +" �: " + calculaDIV(x, y));
	}

	private static int calculaDIV(Integer x, Integer y) {
		if(realizaModulo(x) > realizaModulo(y)) {
			return 1 + calculaDIV(realizaModulo(x) - realizaModulo(y), realizaModulo(y));
		} else if(realizaModulo(x) == realizaModulo(y)) {
			return 1;
		} else {
			return 0;
		}
	}
	
	private static int realizaModulo(Integer x) {
		if(x < 0) {
			return x * -1;
		} else {
			return x;
		}
	}

}
