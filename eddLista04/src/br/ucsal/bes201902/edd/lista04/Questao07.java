package br.ucsal.bes201902.edd.lista04;

public class Questao07 {

	public static void main(String[] args) {
		executa();

	}

	private static void executa() {
		imprimeFibonnaci(20, 1);
		
	}
	
	private static void imprimeFibonnaci(int numeroTermos, int numRep) {
		if(numRep>numeroTermos)
			return;
		else {
			System.out.print(realizaFibonnaci(numRep) + " ");
			imprimeFibonnaci(numeroTermos,++numRep);
		}	
	}

	private static int realizaFibonnaci(int n) {
		if((n == 1) || (n == 2)) {
			return 1;
		} else if (n>2) {
			return realizaFibonnaci(n-1) +  realizaFibonnaci(n-2);
		} else {
			return 0;
		}

	}

}
