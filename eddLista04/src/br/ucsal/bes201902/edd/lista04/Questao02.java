package br.ucsal.bes201902.edd.lista04;

import java.util.Scanner;

public class Questao02 {

	public static Scanner sc = new Scanner(System.in);

	public static final Integer DIVISOR = 2;

	public static void main(String[] args) {
		converterDecimalBinario();

	}

	private static void converterDecimalBinario() {
		System.out.println("Digite o n�mero que voc� deseja converter para bin�rio");
		Integer n = sc.nextInt();
		dec2Bin(n);
	}

	private static void dec2Bin(Integer n) {
		if(n > 0) {
			dec2Bin(n/DIVISOR);
			System.out.print(n%DIVISOR);
		}
	}

}
