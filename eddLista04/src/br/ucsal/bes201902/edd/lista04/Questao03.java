package br.ucsal.bes201902.edd.lista04;

import java.util.Scanner;

public class Questao03 {
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumeroRealizarSoma();

	}

	private static void obterNumeroRealizarSoma() {
		Integer x, y, soma;
		System.out.println("Insira os numeros que voc� deseja somar");
		x = sc.nextInt();
		y = sc.nextInt();
		soma = calculaSoma(x, y);
		imprimeSoma(soma);
	}
	
	private static Integer calculaSucessor(Integer x) {
		// Transforma o x no sucessor
		return ++x;
	}
	
	private static Integer calculaPredecessor(Integer y) {
		// Transforma o y no predecessor
		return --y;
	}

	

	private static Integer calculaSoma(Integer x, Integer y) {
		if(y>0)
			return calculaSoma(calculaSucessor(x),calculaPredecessor(y));
		else if(y<0)
			return calculaSoma(calculaPredecessor(y),calculaSucessor(x));
		else 
			return x;
		
	}
	
	private static void imprimeSoma(Integer soma) {
		System.out.println("Resultado: " + soma);
		
	}

}
