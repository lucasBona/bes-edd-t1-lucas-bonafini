package br.ucsal.bes201902.edd.lista04;

import java.util.Scanner;

public class Questao08 {
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		realizaSoma();

	}

	private static void realizaSoma() {
		Integer n;
		System.out.println("Digite at� que numero inteiro inicial voc� deseja somar(1 a 9)");
		n = sc.nextInt();
		System.out.println(soma(n));
	}

	private static Integer soma(Integer n) {
		if (n == 1) {
			return 1;
		} else {
			return n+soma(n-1);
		}
		
	}

}
