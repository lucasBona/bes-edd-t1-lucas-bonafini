package br.ucsal.bes201902.edd.lista04;

import java.util.Scanner;

public class Questao04 {
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumeroCalcularMDC();

	}

	private static void obterNumeroCalcularMDC() {
		Integer x, y;
		System.out.println("Digite dois n�meros para encontrar o M�ximo Divisor Comum entre eles");
		x = sc.nextInt();
		y = sc.nextInt();
		System.out.println("O MDC entre esses dois n�meros �: " + calculaMDC(x, y));
		
	}

	private static Integer calculaMDC(Integer x, Integer y) {
		if(x > y) {
			return calculaMDC(x-y, y);
		} else if (x == y) {
			return x;
		} else {
			return calculaMDC(y, x);
		}
	}

}
