package br.ucsal.bes201902.edd.lista04;

import java.util.Scanner;

public class Questao05 {
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumeroCalcularMOD();

	}

	private static void obterNumeroCalcularMOD() {
		Integer x, y;
		System.out.println("usu�rio, digite dois n�meros para dividir e encontrar o resto da divis�o entre eles");
		x = sc.nextInt();
		y = sc.nextInt();
		System.out.println("O resto da divis�o de " + x + " por " + y +" �: " + calculaMOD(x, y));
	}

	private static int calculaMOD(Integer x, Integer y) {
		if(realizaModulo(x) > realizaModulo(y)) {
			return calculaMOD(realizaModulo(x) - realizaModulo(y), realizaModulo(y));
		} else if(realizaModulo(x) == realizaModulo(y)) {
			return 0;
		} else {
			return realizaModulo(x);
		}
	}
	
	private static int realizaModulo(Integer x) {
		if(x < 0) {
			return x * -1;
		} else {
			return x;
		}
	}

}
