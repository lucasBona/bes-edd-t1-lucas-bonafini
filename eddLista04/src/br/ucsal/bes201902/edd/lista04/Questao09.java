package br.ucsal.bes201902.edd.lista04;

import java.util.Scanner;

public class Questao09 {
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		realizaSomaSucessiva();

	}

	private static void realizaSomaSucessiva() {
		Integer x, y;
		System.out.println("Usu�rio, digite dois n�meros que voc� deseja multiplicar");
		x = sc.nextInt();
		y = sc.nextInt();
		multiplicacao(x, y);
		System.out.println(x + " * " + y + " = " +multiplicacao(x, y));
	}

	private static Integer multiplicacao(Integer x, Integer y) {
		if(y == 1) {
			return x;
		} else {
			return x + multiplicacao(x, --y);
		}
		
	}

}
