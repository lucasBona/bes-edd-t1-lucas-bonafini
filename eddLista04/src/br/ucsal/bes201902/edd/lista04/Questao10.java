package br.ucsal.bes201902.edd.lista04;

import java.util.Scanner;

public class Questao10 {
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		executa();

	}

	private static void executa() {
		int n;
		System.out.println("A f�rmula do calculo �: (n� + 1)/(n + 3). Que numero voc� deseja substituir no lugar do n?");
		n = sc.nextInt();
		System.out.println(realizaOperacao(n));
		
	}

	private static double realizaOperacao(int i) {
		if(i<=0) {
			return 0;
		} else {
			return ((Math.pow(i, 2)+1)/(i+3)) + realizaOperacao(i-1);
		}	
		
	} 

}
