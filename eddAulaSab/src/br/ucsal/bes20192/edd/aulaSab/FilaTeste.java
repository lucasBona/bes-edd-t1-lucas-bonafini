package br.ucsal.bes20192.edd.aulaSab;

public class FilaTeste {

	public static void main(String[] args) {
		onStarted(new Fila());
	}

	private static void onStarted(IFila fila) {
		fila.inserir(1);
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(4);
		fila.inserir(5);
		fila.inserir(6);
		fila.inserir(7);
		fila.inserir(8);	
		fila.imprimir();
		System.out.println();
		fila.remover();
		fila.remover();
		fila.remover();
		fila.remover();
		fila.imprimir();
		
		
	}

}
