package br.ucsal.bes20192.edd.aulaSab;

public class Fila implements IFila {

	No inicio = null;
	No fim = null;
	
	public void inserir(int valor) {
		No novo = new No();
		novo.valor = valor;
		
		if(inicio == null) {
			inicio = novo;
			fim = novo;
		} else {
			fim.prox = novo;
			fim = novo;
			fim.prox = null;	
		}
	}

	public No remover() {
		No aux = inicio;
		if(inicio != null) {
			inicio = inicio.prox;
		}
		return aux;
	}

	public void imprimir() {
		if(inicio == null) {
			System.out.println("Lista Vazia!");
		} else {
			No aux  = inicio;
			while(aux != null) {
				System.out.print(aux.valor + " ");
				aux = aux.prox;
			}
		}
	}

}
