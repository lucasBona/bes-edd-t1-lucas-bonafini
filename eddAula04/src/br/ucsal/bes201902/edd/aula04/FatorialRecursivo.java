package br.ucsal.bes201902.edd.aula04;

import java.util.Scanner;

public class FatorialRecursivo {

	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obtemNumeroCalculaFatorial();

	}

	public static void obtemNumeroCalculaFatorial() {
		System.out.println("Insira o n�mero que voc� deseja calcular o fatorial");
		int n = sc.nextInt();
		System.out.println("O fatorial de " + n + " �: " +calculaFatorial(n));
		
	}

	private static int calculaFatorial(int n) {
		if(n==0)
			return 1;
		else
			return n*calculaFatorial(n-1);
	}
	

}
