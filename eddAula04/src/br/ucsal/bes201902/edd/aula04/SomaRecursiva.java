package br.ucsal.bes201902.edd.aula04;

import java.util.Scanner;

public class SomaRecursiva {
	
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumeroCalculaSoma();

	}

	private static void obterNumeroCalculaSoma() {
		System.out.println("Insira o n�mero que voc� deseja somar");
		int n = sc.nextInt();
		int soma = soma(n);
		imprime(soma, n);
	}
	
	public static int soma(int n) {
		if(n == 1)
			return 1;
		else
			return n+soma(n-1);
	}
	
	public static void imprime(int n, int i) {
		System.out.println("A soma progressiva de " + i + " �: " + n);
	}

}
