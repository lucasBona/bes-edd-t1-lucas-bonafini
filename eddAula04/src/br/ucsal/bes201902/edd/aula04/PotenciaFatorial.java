package br.ucsal.bes201902.edd.aula04;

import java.util.Scanner;

public class PotenciaFatorial {
	
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumerosCalcularPotencia();

	}

	private static void obterNumerosCalcularPotencia() {
		System.out.println("Insira o numero que voc� deseja elevar");
		int n = sc.nextInt();
		System.out.println("Insira a pot�ncia");
		int p = sc.nextInt();
		long potencia = potencia(n, p);
		imprime(potencia, n, p);
		
	}

	private static void imprime(long potencia, int n, int p) {
		System.out.println(n + "^"+ p +" = "+ potencia);
		
	}

	private static long potencia(int n, int p) {
		if(p == 0)
			return 1;
		else
			return n*potencia(n, (p-1));
		
	}

}
