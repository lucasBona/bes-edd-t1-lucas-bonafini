package br.ucsal.bes201902.edd.aula04;

import java.util.Scanner;

public class Fatorial {
	
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obtemNumeroCalculaFatorial();

	}

	public static void obtemNumeroCalculaFatorial() {
		System.out.println("Insira o n�mero que voc� deseja calcular o fatorial");
		int n = sc.nextInt();
		System.out.println("O fatorial de " + n + " �: " +calculaFatorial(n));
		
	}

	public static int calculaFatorial(int n) {
		int fact = 1;
		for (int i = 1; i <= n; i++) {
			fact *= i;
		}
		
		return fact;
		
	}

}
