package br.ucsal.bes201902.edd.aula06;

public class SelectionSort {

	public static void main(String[] args) {
		executa();

	}

	private static void executa() {
		int[] seq = {7, 0, 4, 9, 6, 1, 2};
		int[] vetorOrdenado = ordenaVetor(seq);
		BubbleSort.imprimir(vetorOrdenado);
	}

	private static int[] ordenaVetor(int[] seq) {
		int i = 0, j =0;
		for (i = 0; i < seq.length; i++) {
			int key = i;
			for (j = i; j < seq.length; j++) {
				if(seq[j] < seq[key]) {
					key = j;
				}
			}
			int temp = seq[i];
			seq[i] = seq[key];
			seq[key] = temp;
		}
		return seq;
	}

}
