package br.ucsal.bes201902.edd.aula06;

public class InsertionSort {

	public static void main(String[] args) {
		executa();

	}

	private static void executa() {
		int[] seq = {7, 0, 4, 9, 6, 1, 2};
		ordenaVetor(seq);
		BubbleSort.imprimir(seq);
		
	}

	private static int[] ordenaVetor(int[] seq) {
		for (int i = 1; i < seq.length; i++) {
			for (int j = i; j > 0 && seq[j] < seq[j-1]; j--) {
					int temp = seq[j - 1];
					seq[j - 1] = seq[j];
					seq[j] = temp;
			}
		}
		return seq;
	}

}
