package br.ucsal.bes201902.edd.aula06;

public class BuscaComOrdenacao {

	public static void main(String[] args) {
		execucao();

	}

	private static void execucao() {
		int[] vetor = {7, 9, 3, 1, 0, 4};
		int numDesejado = 3;
		int[] vetorOrdenado = ordenaVetor(vetor);		
		System.out.println("Vetor: ");
		BubbleSort.imprimir(vetorOrdenado);
		System.out.println();
		System.out.println("index do numero 3: " + buscaBinaria(vetorOrdenado, numDesejado));
		System.out.println(buscaIterativa(vetorOrdenado, numDesejado));
	}

	private static int buscaIterativa(int[] vetorOrdenado, int numDesejado) {
		int cont = 0;
			for(int i=0; i<vetorOrdenado.length; i++, cont++) {
				if(vetorOrdenado[i]==numDesejado) {
					return i;
				}
			}
			System.out.println("Repeti��es" + cont);
			return -1;
		
	}

	private static int buscaBinaria(int[] vetorOrdenado, int numDesejado) {
		int inicio = 0;
		int fim = vetorOrdenado.length - 1;
		int meio = 0;
		int index = -1;
		boolean continua = true;
		int contador = 0;
		while(inicio <= fim && continua) {
			contador++;
			meio = ((fim+inicio)/2) + inicio;
			if(vetorOrdenado[meio] == numDesejado) {
				index = meio;
				continua = false;
			} else if(vetorOrdenado[meio] < numDesejado) {
				inicio = meio+1;
			} else {
				fim = meio -1;
			}
		}
		System.out.println("O n�mero de repeti��es na busca bin�ria foi " + contador);
		return index;

	}

	private static int[] ordenaVetor(int[] vetor) {
		for (int i = 0; i < vetor.length; i++) {
			for (int j = 0; j < vetor.length - 1; j++) {
				if(vetor[j] > vetor[j+1]) {
					int temp = vetor[j];
					vetor[j] = vetor[j+1];
					vetor[j+1] = temp;
				}
			}
		}
		return vetor;

	}

}
