package br.ucsal.bes201902.edd.aula06;


public class BubbleSort {

	public static void main(String[] args) {
		executar();

	}

	private static void executar() {
		int[] array = {7, 0, 4, 9, 6, 1, 2};
		int[] sequenciaOrdenada = ordenarArray(array);
		imprimir(sequenciaOrdenada);
	}

	private static int[] ordenarArray(int[] array) {	
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length -1; j++) {
				if(array[j] > array[j+1]) {
					int menor = array[j];
					array[j] = array[j +1];
					array[j+1] = menor;
				}
			}
		}
		return array;
		
	}
	
	public static void imprimir(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}

}
