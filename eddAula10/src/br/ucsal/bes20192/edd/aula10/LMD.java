package br.ucsal.bes20192.edd.aula10;

public class LMD implements ILMD {

	Noc inicio = null;


	@Override
	public void insere(int codigo, String descricao) {
		Noc novo = new Noc();
		novo.codigo = codigo;
		novo.descricao = descricao;

		if(inicio == null)
			inicio = novo;
		else {
			Noc aux = inicio;
			while(aux.prox!=null) {
				aux = aux.prox;
			}

			aux.prox = novo;
		}


	}

	@Override
	public void remove(int codigo) {
		if(inicio == null)
			System.out.println("Lista vazia!!");
		else {
			Noc ant = null;
			Noc aux = inicio;
			while(aux.prox != null && aux.codigo != codigo) {
				ant = aux;
				aux = aux.prox;
			}
			if(aux.codigo == codigo) {
				if(ant==null) {
					inicio=inicio.prox;
				}else if(aux.prox==null) {
					ant.prox=null;
					aux=null;
				}else {
					ant.prox=aux.prox;
					aux=null;
				}
			}else {
				System.out.println("Categoria n�o encontrada!");
			}
		}
	}



	@Override
	public Noc buscar(int codigo) {
		if(inicio == null) 
			System.out.println("Lista Vazia!!!");
		else {
			Noc aux = inicio;
			while(aux.prox!=null&&aux.codigo!=codigo) {
				aux=aux.prox;
			}
			if(aux.codigo==codigo) {
				return aux;
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

		return null;
	}

	@Override
	public void alterar(int codigo, String descricao) {
		if(inicio == null) 
			System.out.println("Lista Vazia!!!");
		else {
			Noc aux = inicio;
			while(aux.prox != null && aux.codigo != codigo) {
				aux = aux.prox;
			}
			if(aux.codigo == codigo) {
				aux.descricao = descricao;
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}
	}

	@Override
	public void imprimeTudo() {
		if(inicio == null) 
			System.out.println("Lista Vazia!!");
		else {
			Noc aux = inicio;
			while(aux != null) {
				System.out.print(aux.descricao + " ");
				if(aux.ini != null){
					No noAux = aux.ini;
					while(noAux!=null) {
						System.out.print(noAux.valor + " ");
						noAux=noAux.prox;
					}
				}
				aux=aux.prox;
				System.out.print(", ");
			}
		}
		System.out.println(" ");

	}

	@Override
	public void imprimeCategoria(int codigo) {
		if(inicio==null) {
			System.out.println("Lista vazia!");
		}else {
			Noc aux = inicio;
			while(aux.prox != null && aux.codigo != codigo) {
				aux=aux.prox;
			}
			if(aux.codigo==codigo) {
				System.out.print(aux.descricao+" ");
				if(aux.ini!=null){
					No noAux = aux.ini;
					while(noAux!=null) {
						System.out.print(noAux.valor + " ");
						noAux=noAux.prox;
					}
				}
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}
	}



	@Override
	public void insereNo(int codigo, int valor) {

		if(inicio==null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while(nocAux.prox!=null&&nocAux.codigo!=codigo) {
				nocAux=nocAux.prox;
			}
			if(nocAux.codigo==codigo) {
				No noNovo = new No();
				noNovo.valor = valor;

				if(nocAux.ini==null) {
					nocAux.ini = noNovo;
				} else {
					No noAux = nocAux.ini;
					while (noAux.prox != null) {
						noAux = noAux.prox;
					}

					noAux.prox = noNovo;
				}
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

	}

	@Override
	public void removeNo(int valor, int codigo) {
		if(inicio == null) {
			System.out.println("Lista Vazia!!");
		} else {
			Noc aux  = inicio;
			while(aux.prox != null && aux.codigo != codigo) {
				aux = aux.prox;
			}
			if(aux.codigo == codigo) {
				if(aux.ini == null) {
					System.out.println("Elemento n�o encontrado!");
				} else {
					No noAux = aux.ini;
					No noAnt = null;
					while(noAux.prox != null && noAux.valor != valor) {
						noAnt = noAux;
						noAux = noAux.prox;
					} 
					if(noAux.valor == valor) {
						if(noAnt == null) {
							aux.ini = aux.ini.prox;
						} else if(noAux.prox == null) {
							noAnt.prox = null;
							noAux= null;
						} else {
							noAnt.prox = noAux.prox;
							noAux = null;
						}
					} else {
						System.out.println("N� n�o encontrado!");
					}
				}
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

	}

	@Override
	public No buscarNo(int codigo, int valor) {
		if(inicio == null) 
			System.out.println("Lista Vazia!!");
		else {
			Noc aux = inicio;
			while(aux.prox != null && aux.codigo != codigo) {
				aux = aux.prox;
			}
			if(aux.codigo == codigo) {
				if(aux.ini == null){
					System.out.println("Elemento n�o encontrado!");
				} else {
					No noAux = aux.ini;
					while(noAux.prox != null && noAux.valor != valor) {
						noAux = noAux.prox;
					} if(noAux.valor == valor) {
						return noAux;
					} else 
						System.out.println("Elemento n�o encontrado");
				}

			} else 
				System.out.println("Categoria n�o encontrada");
		}
		return null;
	}

	@Override
	public void alterarNo(int codigo, int valor, int novoValor) {
		if(inicio == null)
			System.out.println("Lista Vazia!!!");
		else {
			Noc aux = inicio;
			while(aux.prox != null && aux.codigo != codigo) {
				aux = aux.prox;
			}
			if(aux.codigo == codigo) {
				No noNovo = new No();
				noNovo.valor = valor;

				if(aux.ini == null) {
					System.out.println("N� n�o encontrado");
				} else {
					No noAux = aux.ini;
					while(noAux.prox != null && noAux.valor != valor) {
						noAux = noAux.prox;
					}
					if(noAux.valor == valor) {
						noAux.valor = novoValor;
					} else {
						System.out.println("N� n�o encontrado");
					}
				}

			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}	

	}


}