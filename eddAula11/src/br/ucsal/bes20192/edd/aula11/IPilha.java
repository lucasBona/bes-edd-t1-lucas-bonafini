package br.ucsal.bes20192.edd.aula11;

public interface IPilha {
	void push(int valor);
	No pop();
	void imprimir();
}
