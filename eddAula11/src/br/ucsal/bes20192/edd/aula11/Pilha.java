package br.ucsal.bes20192.edd.aula11;

public class Pilha implements IPilha {

	No topo = null;

	@Override
	public void push(int valor) {
		No novo = new No();
		novo.valor = valor;

		if(topo == null) 
			topo = novo;
		else {
			novo.prox = topo;
			topo = novo;		
		}

	}

	@Override
	public No pop() {
		No aux = topo;
		if(topo != null) {
			topo = topo.prox;
		}	
		return aux;
	}

	@Override
	public void imprimir() {
		if(topo == null) {
			System.out.println("Pilha vazia");
		} else {
			No aux = topo;
			while(aux!=null) {
				System.out.print(aux.valor + " ");
				aux=aux.prox;
			}
		}

	}

}
