package br.ucsal.bes20192.edd.lista06;

public interface IFila {
	void inserir(int valor);
	No remover();
	void imprimir();
}
