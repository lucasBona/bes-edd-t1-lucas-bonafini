package br.ucsal.bes20192.edd.lista06;

public class Questoes {
    public static void main(String[] args) {
        executa(new Fila());
    }

    public static void executa(IFila fila) {
        fila.inserir(1);
        fila.inserir(2);
        fila.inserir(3);
        fila.inserir(4);
        fila.inserir(5);
        fila.inserir(6);
        fila.inserir(7);

        System.out.println("Questao 01:\nO tamanho da fila "+questao01(fila)+"\n");
        System.out.println("Esperado: 7");

        System.out.println("Questao 02:\nA fila de entrada:");
        fila.imprimir();
        System.out.println("A nova fila:");
        fila = questao02(fila);
        fila.imprimir();

        Fila flaTetra = new Fila();
        flaTetra.inserir(10);
        flaTetra.inserir(12);
        flaTetra.inserir(13);
        flaTetra.inserir(14);

        System.out.println("\nQuestao 03:\nA fila 1 :");
        fila.imprimir();
        System.out.println("A fila 2 : ");
        flaTetra.imprimir();
        System.out.println("A fila resultante :");
        flaTetra = questao03(fila,flaTetra);
        flaTetra.imprimir();

        System.out.println("\nQuestao 04:\nA fila que entra:");
        flaTetra.imprimir();
        System.out.println("A fila que sai :");
        flaTetra = questao04(flaTetra);
        flaTetra.imprimir();





    }

    private static int questao01(IFila fila) {
        Fila filaAux = new Fila();
        filaAux = (Fila) fila;
        int tamanho = 0;
        if(filaAux.inicio == null) {
            return tamanho;
        } else {
            No aux = filaAux.inicio;
            while (aux != null) {
                aux = aux.prox;
                tamanho++;
            }
        }
        return tamanho;
    }

    private static Fila questao02(IFila fila) {
        Fila filaAux = (Fila) fila;
        int[] vetor = new int[5];
        int count = 0;
        while(count <= 4){
            vetor[count] = filaAux.inicio.valor;
            filaAux.remover();
            count++;
        }
        for(int i =4; i >= 0; i--) {
            filaAux.inserir(vetor[i]);
        }

        return filaAux;

    }
    private static Fila questao03(IFila filaNormal, Fila filaPreferencial) {
        Fila filaAux = (Fila) filaNormal;
        Fila filaRetorno = new Fila();
        int count = 0;
        No auxFilaNormal = filaAux.inicio;
        No auxFilaPreferencial = filaPreferencial.inicio;
        while(auxFilaNormal != null || auxFilaPreferencial != null) {
            if(count < 3){
                if(auxFilaNormal != null){
                    filaRetorno.inserir(auxFilaNormal.valor);
                    auxFilaNormal = auxFilaNormal.prox;
                    count++;
                } else {
                    if(auxFilaPreferencial != null){
                        filaRetorno.inserir(auxFilaPreferencial.valor);
                        auxFilaPreferencial = auxFilaPreferencial.prox;
                    }
                    count = 0;
                }
            } else {
                filaRetorno.inserir(auxFilaPreferencial.valor);
                auxFilaPreferencial = auxFilaPreferencial.prox;
                count = 0;
            }

        }
        return filaRetorno;
    }

    private static Fila questao04(IFila filinha) {
        Fila filaAux = (Fila) filinha;
        Fila filaRetorno = new Fila();
        int tamanho = questao01(filinha);
        int[] vetor = new int[tamanho];
        No aux = filaAux.inicio;

        for(int i= tamanho; i > 0; i--){
            vetor[i-1] = aux.valor;
            aux = aux.prox;
        }

        for (int i = 0; i<tamanho; i++) {
            filaRetorno.inserir(vetor[i]);
        }

        return filaRetorno;
    }
}
