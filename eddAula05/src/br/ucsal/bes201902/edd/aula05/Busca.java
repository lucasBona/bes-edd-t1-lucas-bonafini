package br.ucsal.bes201902.edd.aula05;

public class Busca {

	public static void main(String[] args) {
		executa();

	}

	private static void executa() {
		int[] array = {0, 2, 3, 5, 7, 8, 9, 10, 15};
		int numDesejado = 1;
		if(buscaIndex(array, numDesejado) == -1) {
			System.out.println("N�o existe esse n�mero no array");
		} else {
			System.out.println(buscaIndex(array, numDesejado));
		}	
	}

	private static int buscaIndex(int[] array, int numDesejado) {
		int inicio = 0;
		int fim = array.length - 1;
		int meio = 0;
		int index = -1;
		boolean continua = true;
		while(inicio <= fim && continua) {
			meio = ((fim-inicio)/2) + inicio;
			if(array[meio] == numDesejado) {
				index = meio;
				continua = false;
			} else if(array[meio] < numDesejado) {
				inicio = meio + 1;
			} else {
				fim = meio -1;
			}
			
		}
		return index;
		
	}

}
