package br.ucsal.bes201902.edd.aula05;

public class BuscaRecursiva {

	public static void main(String[] args) {
		executa();

	}

	private static void executa() {
		int[] array = {0, 2, 3, 5, 7, 8, 9, 10, 15};
		int numDesejado = 15;
		System.out.println(buscaIndex(array, numDesejado, 0, array.length - 1));
	}

	private static int buscaIndex(int[] array, int numDesejado, int inicio, int fim) {
		if(fim<inicio)
			return -1;
		
		int meio = ((fim-inicio)/2)+inicio;
		
		if(array[meio]==numDesejado) {
			return meio;
		} else {
			if(numDesejado<array[meio]) {
				return buscaIndex(array, numDesejado , inicio, meio-1 );
			}else {
				return buscaIndex(array, numDesejado, meio+1, fim);				
			}
		}
		
	}

}
