package br.ucsal.bes201902.edd.aula09;

public class LC implements ILC {

	No inicio = null;
	No fim = null;

	@Override
	public void inserir(int valor) {
		
		No novo = new No();
		novo.valor = valor;

		if(inicio==null) {
			
			inicio = novo;
			fim = novo;
			fim.prox = inicio;
			
		} else {
			No ant = null;
			No aux = inicio;
			while(aux != fim && valor > aux.valor) {
				ant = aux;
				aux = aux.prox;
			}
			if(ant == null)
			fim.prox = novo;
			fim = novo;
			fim.prox = inicio;
			
		}

	}

	@Override
	public void remover(int chave) {
		
		if(inicio==null) {

			System.out.println("Lista VAzia!");

		} else {
			No ant = null;
			No aux = inicio;
			
			while(aux.prox != inicio &&aux.valor != chave) {
				ant = aux;
				aux = aux.prox;
				
			}
			
			if(aux.valor == chave) {
				
				if(ant == null) {
					inicio = inicio.prox;
					fim.prox = inicio;
					aux.prox = null;
					aux = null;				
				} else if(aux.prox == inicio) {
					ant.prox = inicio;
					fim = ant;
					aux.prox = null;
					aux = null;
				} else {
					ant.prox = aux.prox;
					aux.prox = null;
					aux = null;
				}
				
			} else {
				System.out.println("elemento n�o encontrador");
			}
		}

	}

	@Override
	public void alterar(int chave, int novoValor) {

		if(inicio==null) {

			System.out.println("Lista VAzia!");

		} else {

			No aux = inicio;
			
			while(aux.prox != inicio && aux.valor != chave) {
				
				aux = aux.prox;
				
			}

			if(aux.valor == chave) {
				
				aux.valor = novoValor;

			} else {
				
				System.out.println("Elemento n�o encontrado");
				
			}
			
		}

	}

	@Override
	public No buscar(int chave) {
		
		if(inicio==null) {

			System.out.println("Lista VAzia!");

		} else {

			No aux = inicio;
			
			while(aux.prox != inicio && aux.valor != chave) {
				
				aux = aux.prox;
				
			}

			if(aux.valor == chave) {
				
				return aux;

			} else {
				
				System.out.println("Elemento n�o encontrado");
				
			}
			
		}

		
		return null;
	}

	@Override
	public void imprimir() {
		
		if(inicio==null) {
			System.out.println("Lista VAzia!");
		} else {
			
			No aux = inicio;
			
			do {
				
				System.out.print(aux.valor + " ");
				aux = aux.prox;
				
			} while(aux != inicio);
			
		}

				
			

	}

	@Override
	public void imprimirDuasVezes() {
		if(inicio==null) {
			System.out.println("Lista VAzia!");
		} else {
			
			No aux = inicio;
			int count = 0;
			
			do {
				
				System.out.print(aux.valor + " ");
				aux = aux.prox;
				if(aux == inicio) 
					count++;
				
			} while(count < 2);
			
		}

	}

}
