package br.ucsal.bes201909.edd.aula02;

import java.util.Scanner;

public class Vetor implements IVetor {
	
	public static Scanner sc = new Scanner(System.in);
	public static int qtd = 0;
	public static final int QTD_ITENS = 3;
	
	int[] numeros = new int[QTD_ITENS];

	public int produtoEscalar(int valor) {
		int soma = 0;
		for (int i = 0; i < qtd; i++) {
			soma += numeros[i] * valor;
		}
		return soma;
	}

	public int somaDosElementos() {
		int soma1 = 0;
		for (int i = 0; i < qtd; i++) {
			soma1 += numeros[i];
		}
		return soma1;
	}

	public int numeroDeElementos() {
		return qtd;
	}

	public void leitura() {
		String escolha;
		System.out.println("Usu�rio, digite at� "+ QTD_ITENS + " n�meros inteiros");
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = sc.nextInt();
			qtd++;
			if(i < numeros.length){
				System.out.println("Deseja parar de inserir valores? S/N");
				escolha = sc.next().toUpperCase();
				if(escolha.equals("S")) {
					break;
				}
			}	
		}
	}

	public void imprime() {
		System.out.println("A sequ�ncia �: ");
		for (int i = 0; i < qtd; i++) {
			System.out.print(numeros[i] + " ");
		}
		
		System.out.println();

	}

}
