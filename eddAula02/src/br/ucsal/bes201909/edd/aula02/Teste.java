package br.ucsal.bes201909.edd.aula02;

import java.util.Scanner;

public class Teste {
	
	
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		executa(new Vetor());

	}
	
	public static void executa(IVetor vetor) {
		int valor;
		vetor.leitura();
		vetor.imprime();
		System.out.println("Insira um valor para realizar o produto escalar");
		valor = sc.nextInt();
		System.out.println("Seu produto escalar �: " + vetor.produtoEscalar(valor));
		System.out.println("A soma dos elementos desse vetor �: " + vetor.somaDosElementos());
		
	}

}
