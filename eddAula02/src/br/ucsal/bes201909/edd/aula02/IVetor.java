package br.ucsal.bes201909.edd.aula02;

public interface IVetor {
	int produtoEscalar(int valor);
	int somaDosElementos();
	int numeroDeElementos();
	void leitura();
	void imprime();

}
