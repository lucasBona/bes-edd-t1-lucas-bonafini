package br.ucsal.bes201902.edd.aula03;

import java.util.Scanner;

public class String implements IString {
	char[] valores;
	public String(int tamanho) {
		valores = new char[tamanho];
	}

	public String() {
		Scanner entrada = new Scanner(System.in);
		valores = entrada.nextLine().toCharArray();
	}

	public int length() {
		int tamanho = valores.length;
		return tamanho;
	}

	public char charAt(int posicao) {
		return valores[posicao];
	}

	public boolean equals(String valor) {
		boolean valorIgual = true;
		if(valores.length != valor.length()) {
			valorIgual = false;
			return valorIgual;
		} else {
			for (int i = 0; i < valor.length(); i++) {
				if(valor.valores[i] == valores[i]) {
					valorIgual = true;
				} else {
					valorIgual = false;
					return valorIgual;
				}
			}
		}
		return valorIgual;
	}

	public boolean startsWith(String valor) {
		boolean valorIgual = true;
		if(valores.length < valor.length()) {
			valorIgual = false;
			return valorIgual;
		} else {
			for (int i = 0; i < valor.length(); i++) {
				if(valor.valores[i] == valores[i]) {
					valorIgual = true;
				} else {
					valorIgual = false;
					return valorIgual;
				}
			}
			return valorIgual;
		}

	}

	public boolean endWith(String valor) {
		boolean valorIgual = true;
		if(valores.length < valor.length()) {
			valorIgual = false;
			return valorIgual;
		} else {
			for (int i = valor.length(), posicao = valores.length; i > 0; i--, posicao--) {
				if(valor.valores[i] == valores[posicao]) {
					valorIgual = true;
				} else {
					valorIgual = false;
					return valorIgual;
				}
			}
			return valorIgual;
		}

	}

	public int indexOf(char letra) {
		int posicao = -1;
		for (int i = 0; i < valores.length; i++) {
			if(letra == valores[i]) {
				posicao = i;
				return posicao;
			}
		}
		return posicao;
	}

	public int lastIndexOf(char letra) {
		int posicao = -1;
		for (int i = 0; i < valores.length; i++) {
			if(letra == valores[i]) {
				posicao = i;
			}
		}
		return posicao;
	}

	public String substring(int inicio, int quantidadeDeCaracteres) {
		String texto = new String(quantidadeDeCaracteres);
		for (int i = inicio; i < quantidadeDeCaracteres; i++) {
			texto.valores[i] = valores[i];
		}
		return texto;
	}

	public String replace(char letraASerTrocada, char letraATrocar) {
		for (int i = 0; i < valores.length; i++) {
			if(valores[i] == letraASerTrocada) {
				valores[i] = letraATrocar;
			}
		}
		return this;
	}

	public String concat(String valor) {
		String texto = new String(valores.length + valor.length());
		int num = 0;
		for (int i = 0; i < valores.length; i++) {
			texto.valores[i] = valores[i];
		}
		for (int i = valores.length; i < texto.length()+valor.length(); i++, num++) {
			texto.valores[i] = valor.valores[num];
			System.out.print(texto.valores[i]);
		}
		return texto;
	}

	public void imprime() {
		System.out.print("A palavra �: ");
		for (int i = 0; i < valores.length; i++) {
			System.out.print(valores[i]);
		}
	}



}
