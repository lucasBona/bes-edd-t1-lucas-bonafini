package br.ucsal.bes20192.edd.lista05;

public class Questoes {


    public static void main(String[] args) {
        executa(new Pilha());

    }

    public static void executa(Pilha P1) {
        P1 = Pilhando.pilhando1();
        Pilha P2 = Pilhando.pilhando2();

        questao01(P1, P2);

        P1 = Pilhando.pilhando2();
        P2 = Pilhando.pilhando1();

        questao01(P1, P2);

        P1 = Pilhando.pilhando3();
        P2 = Pilhando.pilhando1();

        questao01(P1, P2);

        P1 = Pilhando.pilhando1();
        P1.imprimir();
        P2 = questao02(P1);
        P2.imprimir();

        P1 = Pilhando.pilhando1();
        P2 = Pilhando.pilhando3();

        System.out.println("A resposta deve ser falso: \n"+questao03(P1, P2));

        P1 = Pilhando.pilhando1();
        P2 = Pilhando.pilhando2();

        System.out.println("A resposta deve ser falso: \n"+questao03(P1, P2));

        P1 = Pilhando.pilhando1();
        P2 = Pilhando.pilhando4();

        System.out.println("A resposta deve ser falso: \n"+questao03(P1, P2));

        P1 = Pilhando.pilhando1();
        P2 = Pilhando.pilhando1();

        System.out.println("A resposta deve ser verdadeiro: \n"+questao03(P1, P2));

    }


    private static int tamanho(Pilha pilha) {
        int count = 0;
        if (pilha.topo == null) {
            System.out.println("Lista vazia!");
            return 0;
        } else {
            count++;
            while (pilha.pop() != null) {
                count++;

            }
        }
        return count;
    }

    private static void questao01(Pilha P1, Pilha P2) {
        int P1Size = tamanho(P1);
        int P2Size = tamanho(P2);

        if (P1Size > P2Size) {
            System.out.println("A pilha 1 e maior!");
        } else if (P2Size > P1Size) {
            System.out.println("A pilha 2 e maior!");
        } else {
            System.out.println("As duas pilhas sao iguais");
        }
    }

    private static Pilha questao02(Pilha p) {
        Pilha aux = new Pilha();

        if (p.topo == null) {
            System.out.println("A pilha esta vazia!");
            return null;
        } else {
            while (p.topo.prox != null) {
                aux.push(p.topo.valor);
                p.pop();
            }
            aux.push(p.topo.valor);

            p = aux;
            return p;
        }
    }

    public static boolean questao03(Pilha p1, Pilha p2) {
        if(p1.topo == null || p2.topo == null) {
            System.out.println("Pelo menos uma pilha esta vazia");
        } else {
            while (p1.topo.prox != null) {
                if(p1.topo.valor == p2.topo.valor){
                    p1.pop();
                    p2.pop();
                } else {
                    return false;
                }
            }
            if(p1.topo.valor == p2.topo.valor) {
                p1.pop();
                p2.pop();
            } else if (p2.topo.prox != null) {
                return false;
            }

        }
        return true;
    }

}
