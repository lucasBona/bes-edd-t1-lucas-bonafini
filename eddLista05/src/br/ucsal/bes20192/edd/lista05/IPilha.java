package br.ucsal.bes20192.edd.lista05;

public interface IPilha {
	void push(int valor);
	No pop();
	void imprimir();
}
