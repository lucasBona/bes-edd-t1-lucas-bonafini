package br.ucsal.bes20192.edd.lista05;

public class TesteQuestao01 {

	public static void main(String[] args) {
		executa();

	}

	private static void executa() {
		Pilha pilha1 = new Pilha();
		Pilha pilha2 = new Pilha();

		pilha1.push(1);
		pilha1.push(4);
		pilha1.push(2);
		pilha1.push(7);
		pilha1.push(9);

		pilha2.push(1);
		pilha2.push(4);
		pilha2.push(2);
		pilha2.push(7);
		pilha2.push(9);
		
		pilha1.imprimir();
		System.out.println("----------------------");
		pilha2.imprimir();
		System.out.println("----------------------");
		System.out.println("Esperado: as pilhas tem tamanhos iguais");
		comparaPilha(pilha1, pilha2);
		System.out.println("----------------------");
		
		pilha1.push(1);
		pilha1.push(4);
		pilha1.push(2);
		pilha1.push(7);
		pilha1.push(9);

		pilha2.push(1);
		pilha2.push(4);
		pilha2.push(2);
		pilha2.push(7);
		pilha2.push(9);
		
		pilha2.pop();
		pilha1.imprimir();
		System.out.println("----------------------");
		pilha2.imprimir();
		System.out.println("----------------------");
		System.out.println("Esperado: pilha1 � maior");
		comparaPilha(pilha1, pilha2);
		System.out.println("----------------------");
		
		pilha1.push(1);
		pilha1.push(4);
		pilha1.push(2);
		pilha1.push(7);
		pilha1.push(9);

		pilha2.push(1);
		pilha2.push(4);
		pilha2.push(2);
		pilha2.push(7);

		pilha1.pop();
		pilha1.pop();
		
		pilha1.imprimir();
		System.out.println("----------------------");
		pilha2.imprimir();
		System.out.println("----------------------");
		System.out.println("Esperado: pilha2 � maior");
		comparaPilha(pilha1, pilha2);
	}
	
	public static int tamanho(Pilha pilha) {
		int count = 0;
		if(pilha.topo == null) {
			return count;
		} else {
			while(pilha.pop() != null) {
				count++;
			}
		}
		return count;
	}
	
	public static void comparaPilha(Pilha p1, Pilha p2) {
		int p1Size = tamanho(p1);
		int p2Size = tamanho(p2);
		
		if(p1Size > p2Size) {
			System.out.println("A pilha 1 � maior");
		} else if(p1Size == p2Size) {
			System.out.println("As pilhas tem tamanhos iguais");
		} else {
			System.out.println("A pilha 2 � maior");
		}
	}


}
