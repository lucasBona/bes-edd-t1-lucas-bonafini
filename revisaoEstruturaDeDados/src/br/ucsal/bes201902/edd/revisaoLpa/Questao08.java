package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;

public class Questao08 {
	
	private static final int QNT_ITENS = 3;
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		obterEImprimirImposto();
	}
	
	public static void obterEImprimirImposto() {
		System.out.println("Insira o nome e os sal�rios respectivamente");
		String[] nomes = new String[QNT_ITENS];
		int[] salarios = new int[QNT_ITENS];
		for (int i = 0; i < salarios.length; i++) {
			nomes[i] = sc.next();
			salarios[i] = sc.nextInt();
		}
		
		for (int i = 0; i < salarios.length; i++) {
			System.out.println("O sal�rio de " + nomes[i] + " � de R$" + salarios[i]);
			if(salarios[i] >= 0 && salarios[i] < 600) {
				System.out.println("Sal�rio menor que R$600,00. " + nomes[i] + " est� isento do imposto de renda");
			} else if(salarios[i] >= 600 && salarios[i] < 1500) {
				System.out.println("Sal�rio maior que R$600,00 e menor que R$1500,00 " + nomes[i] + " ter� 10% do seu sal�rio bruto convertido em impostos");
			} else if(salarios[i] >= 1500){
				System.out.println("Sal�rio maior que R$1500,00. " + nomes[i] + " ter� 15% do seu sal�rio bruto convertido em impostos");
			} else {
				System.out.println("Sal�rio inv�lido");
			}
		}
	}
}