package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;

public class Questao12 {
	
	public static Scanner sc  = new Scanner(System.in);
	public static final int QTD_ITENS = 10;

	public static void main(String[] args) {
		obterNumeroRealizarSequencia();

	}

	private static void obterNumeroRealizarSequencia() {
		int[] seq;
		int inicial = obterInicial();
		int razao = obterRazao();
		seq = fazerPG(inicial, razao);
		imprimirPG(seq);
		
	}

	private static void imprimirPG(int[] seq) {
		for (int i = 0; i < seq.length; i++) {
			System.out.print(seq[i] + " ");
		}
		
	}

	private static int[] fazerPG(int inicial, int razao) {
		int[] pa = new int[QTD_ITENS];
		int in = inicial;
		int r = razao;
		for (int i = 0; i < pa.length; i++) {	
			pa[i] = in;
			in = in*r;
		}
		
		return pa;
	}

	private static int obterRazao() {
		System.out.println("Usu�rio, digite a raz�o da PG");
		int razao = sc.nextInt();
		
		return razao;
	}

	private static int obterInicial() {
		System.out.println("Usu�rio, digite o numero inicial da sequ�ncia geom�trica");
		int inicial = sc.nextInt();
		
		return inicial;
	}

}
