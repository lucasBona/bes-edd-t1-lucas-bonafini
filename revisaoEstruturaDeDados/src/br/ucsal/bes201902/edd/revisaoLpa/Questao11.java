package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;

public class Questao11 {
	
	public static Scanner sc  = new Scanner(System.in);
	public static final int QTD_ITENS = 10;

	public static void main(String[] args) {
		obterNumeroRealizarSequencia();

	}

	private static void obterNumeroRealizarSequencia() {
		int[] seq;
		int inicial = obterInicial();
		int razao = obterRazao();
		seq = fazerPA(inicial, razao);
		imprimirPA(seq);
		
	}

	private static void imprimirPA(int[] seq) {
		for (int i = 0; i < seq.length; i++) {
			System.out.print(seq[i] + " ");
		}
		
	}

	private static int[] fazerPA(int inicial, int razao) {
		int[] pa = new int[QTD_ITENS];
		int seq = inicial;
		for (int i = 0; i < pa.length; i++) {	
			pa[i] = seq;
			seq += razao;
		}
		
		return pa;
	}

	private static int obterRazao() {
		System.out.println("Usu�rio, digite a raz�o da PA");
		int razao = sc.nextInt();
		
		return razao;
	}

	private static int obterInicial() {
		System.out.println("Usu�rio, digite o numero inicial da sequ�ncia aritm�tica");
		int inicial = sc.nextInt();
		
		return inicial;
	}

}
