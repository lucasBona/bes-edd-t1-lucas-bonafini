package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;

public class Questao03 {
	
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		inicio();

	}
	
	public static void inicio() {
		System.out.println("Usu�rio, insira o primeiro valor");
		double x = sc.nextDouble();
		System.out.println("A opera��o");
		String op = sc.next();
		System.out.println("O segundo valor");
		double y = sc.nextDouble();
		operation(x, op, y);
	}
	
	public static void operation(double x, String op, double y) {
		double res;
		if(op.contentEquals("+")) {
			res = x + y;
			System.out.println(x + op + y + "= " + res);	   
		} else if(op.contentEquals("-")) {
			res = x - y;
			System.out.println(x + op + y + "= " + res);
		} else if(op.contentEquals("*")) {
			res = x*y;
			System.out.println(x + op + y + "= " + res);
		} else if(op.contentEquals("/")) {
			res = x/y;
			System.out.println(x + op + y + "= " + res);
		} else {
			System.out.println("Opera��o inv�lida");
		}
	}

}