package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;
import java.text.DecimalFormat;

public class Questao02 {

	public static Scanner sc = new Scanner(System.in);
	public static DecimalFormat df2 = new DecimalFormat("#.##");
	
	public static void main(String[] args) {
		inicio();

	}
	
	public static void inicio() {
		String name;
		int choices;
		double weight, height, IMC;
		System.out.println("Usu�rio, bem vindo ao c�lculador de IMC, informe seu nome, peso e altura respectivamente");
		name = sc.next();
		System.out.println("Agora o peso");
		weight = sc.nextDouble();
		System.out.println("Agora sua altura");
		height = sc.nextDouble();
		IMC = calculateIMC(weight, height);
		choices = choicesTree(IMC);
		print_results(name, IMC, choices);
	}
	
	public static double calculateIMC(double weight, double height) {
		double IMC = weight/(height*height);
		return IMC;
	}
	
	public static int choicesTree(double IMC) {
		int num = 0;
		if (IMC < 18.5) {
			num = 1;
		} else if (IMC >= 18.5 && IMC < 25) {
			num = 2;
		} else if (IMC >= 25 && IMC < 30) {
			num = 3;
		} else {
			num = 4;
		}
		return num;
	}
	
	public static void print_results (String name, double IMC, int num) {
		if (num == 1) {
			System.out.println(name + " seu IMC � " + df2.format(IMC) + " voc� est� abaixo do peso");
		} else if (num == 2) {
			System.out.println(name + " seu IMC � " + df2.format(IMC) + " voc� est� com o peso normal");
		} else if (num == 3) {
			System.out.println(name + " seu IMC � " + df2.format(IMC) + " voc� est� acima do peso");
		} else if (num == 4) {
			System.out.println(name + " seu IMC � " + df2.format(IMC) + " voc� est� com obesidade");
		}
	}

}
