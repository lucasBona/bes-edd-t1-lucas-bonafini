package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;

public class Questao01 {

	public static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		inicio();

	}
	
	public static void inicio() {
		double temperature, convTemp;
		System.out.println("Usu�rio, informe a temperatura em �C que voc� deseja converter para �F");
		temperature = sc.nextDouble();
		convTemp = operation(temperature);
		print(convTemp);
	}
	
	public static double operation(double temp) {
		return (temp*1.8) + 32;
	}
	
	public static void print(double temp) {
		System.out.println("A temperatura em Graus Farenheint � " + temp + " �F." );
	}

}
