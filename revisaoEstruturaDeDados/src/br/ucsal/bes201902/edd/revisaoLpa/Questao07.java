package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;

public class Questao07 {
	
	private static final int QTD_ITENS = 10;
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterVetorRealizarProduto();

	}

	private static void obterVetorRealizarProduto() {
		int[] vet1;
		int[] vet2;
		int[] vet3;
		
		vet1 = obterNumeros();
		vet2 = obterNumeros();
		vet3 = multiplicarVetores(vet1, vet2);
		exibirVetor(vet3);
	}
	
	public static int[] obterNumeros() {
		int[] vet = new int[QTD_ITENS];
		System.out.println("Insira " + QTD_ITENS + " n�meros");
		for (int i = 0; i < vet.length; i++) {
			vet[i] = sc.nextInt();
		}
		
		return vet;
	}
	
	public static int[] multiplicarVetores(int[] vet1, int[] vet2) {
		int[] multiplicacao = new int[QTD_ITENS];
		for (int i = 0; i < multiplicacao.length; i++) {
			multiplicacao[i] = vet1[i]*vet2[i];
		}
		
		return multiplicacao;
	}
	
	public static void exibirVetor(int[] multiplicacao) {
		System.out.println("A multiplica��o dos respectivos �ndices �: ");
		for (int i = 0; i < multiplicacao.length; i++) {
			System.out.print(multiplicacao[i] + " ");
		}
	}

}
