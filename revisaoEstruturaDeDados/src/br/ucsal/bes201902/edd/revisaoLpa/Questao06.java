package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;

public class Questao06 {
	
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		inicio();

	}
	
	public static void inicio() {
		int[] vet = new int [5];
		int[] sequence = getSeq(vet);
		System.out.println();
		print(sequence);
		
	}
	
	public static int[] getSeq(int[] seq) {
		for (int i = 0; i < seq.length; i++) {
			seq[i] = sc.nextInt();
		}
		return seq;
	}
	
	public static void print(int[] seq) {
		for (int i = seq.length - 1; i >= 0; i--) {
			System.out.println(seq[i]);
		}
	}

}
