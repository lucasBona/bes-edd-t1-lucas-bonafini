package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Scanner;

public class Questao09 {
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		realizarRaizQuadrada();

	}
	
	public static void realizarRaizQuadrada() {
		Integer n;
		int raiz;
		n = obterNumero();
		raiz = realizarCalculo(n);
		exibirResultado(raiz);
	}
	
	public static Integer obterNumero() {
		Integer n;
		System.out.println("Insira o n�mero que voc� deseja realizar o c�lculo da raiz quadrada");
		n = sc.nextInt();
		return n;
		
	}
	
	public static Integer realizarCalculo(Integer n) {
		int i = 0;
		for (int impar = 1; n > 0; i++, impar += 2) {
			n -= impar;
		}
		
		return i;
	}
	
	public static void exibirResultado(int i) {
		System.out.println("O resultado da raiz quadrada � "+ i);
	}

}
