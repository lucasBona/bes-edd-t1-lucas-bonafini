package br.ucsal.bes201902.edd.revisaoLpa;

import java.util.Random;
import java.util.Scanner;

public class Questao13 {
	
	public static Scanner sc = new Scanner(System.in);
	public static final int QTD_ITENS = 10;

	public static void main(String[] args) {
		sortearNumeroEscolherNumero();

	}

	private static void sortearNumeroEscolherNumero() {
		int[] vet1;
		int numeroSorteado, casaCerta;
		boolean numeroExiste;
		vet1 = obterNumeros();
		numeroSorteado = inserirNumero();
		casaCerta = exibirNumerosSorteados(numeroSorteado, vet1);
		numeroExiste = verificarSeNumeroFoiSorteado(numeroSorteado, vet1);
		System.out.println();
		exibirResultados(numeroExiste, casaCerta);
		
	}

	private static void exibirResultados(boolean numeroExiste, int casa) {
		if(numeroExiste) {
			System.out.println("Parab�ns, voc� acertou o n�mero e est� na " + (casa + 1) + "� casa");
		} else {
			System.out.println("N�o foi dessa vez");
		}
		
	}

	private static boolean verificarSeNumeroFoiSorteado(int numeroSorteado, int[] vet1) {
		boolean numeroExiste = false;
		for (int j = 0; j < vet1.length; j++) {
			if(numeroSorteado == vet1[j]) {
				numeroExiste = true;
			}
		}
		
		return numeroExiste;
	}

	private static int exibirNumerosSorteados(int numeroSorteado, int[] vet) {
		int casa = 0;
		for (int i = 0; i < vet.length; i++) {
			System.out.print(vet[i] + " ");
			if(numeroSorteado == vet[i]) {
				casa = i;
			}
		}
		
		return casa;
	}

	private static int inserirNumero() {
		int escolha;
		System.out.println("Usu�rio, digite um n�mero para ver se ele foi sorteado");
		escolha = sc.nextInt();
		
		return escolha;
	}

	private static int[] obterNumeros() {
		int[] vet = new int[QTD_ITENS];
		Random rd = new Random();
		for (int i = 0; i < vet.length; i++) {
			vet[i] = rd.nextInt(10);
		}
		return vet;
	}

}
