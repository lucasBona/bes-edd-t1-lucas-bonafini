package br.ucsal.bes201902.edd.revisaoLpa;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Questao04 {

	public static Scanner sc = new Scanner(System.in);
	public static DecimalFormat df2 = new DecimalFormat("#.##");

	public static void main(String[] args) {
		inicio();

	}

	public static void inicio() {
		double grown, worldPop;
		System.out.println("POPULAÇÃO MUNDIAL = ");
		worldPop = sc.nextDouble();
		System.out.println("TAXA DE CRESCIMENTO DEMOGRÁFICO(%)");
		grown = sc.nextDouble();
		op(worldPop, grown);
	}

	public static double op (double pop, double grown) {
		double tax, year;
		for (int i = 1; i <= 5; i++) {
			tax = (grown*pop)/100;
			year = pop + tax;
			pop = year;
			System.out.println(i + "º ano = " + df2.format(pop));
		}
       return pop;
	}

}
