package br.ucsal.bes201902.edd.aula07;

import java.util.Scanner;

public class Crud {

	private static Scanner sc = new Scanner(System.in);

	private static String[] vetor = new String[5];

	public static void main(String[] args) {
		crud();

	}

	private static void crud() {
		int escolha = mostraMenu();
		
		switch(escolha) {
		case 1:
			inserirValor();
			break;
		case 2:
			imprimirVetor();
			break;
		case 3:
			alterarVetor();
			break;
		case 4:
			deletarValor();
			break;
		case 5:
			System.out.println("Adeus...");
			break;
		default:
			break;


		}

	}

	private static void deletarValor() {
		System.out.println("Digite a posi��o que voc� deseja apagar");
		int posicao = sc.nextInt();
		for (int i = posicao; i < vetor.length - 1; i++) {
			vetor[i] = vetor[i+1];
		}
		
		
	}

	private static void alterarVetor() {
		imprimirVetor();
		System.out.println("Digite a posi��o que voc� deseja substituir");
		int posicao = sc.nextInt();
		System.out.println("Agora a letra que voc� deseja botar no lugar");
		String letra = sc.nextLine();
		vetor[posicao] = letra;
		
	}

	private static void inserirValor() {
		String letra;
		int i = 0;
		boolean continua = true;
		while(continua || i < vetor.length) {
			if(i == vetor.length) {
				System.out.println("Vetor lotado");
				continua = false;
			} else {
				System.out.println("Coloque a letra que deseja inserir");
				letra = sc.nextLine();
				vetor[i] = letra;
				continua = false;
			}			
			i++;
			crud();

		}
	}

	private static void imprimirVetor() {
		for (int i = 0; i < vetor.length; i++) {
			System.out.print(vetor[i] + " ");
		}

	}

	private static int mostraMenu() {
		int escolha = 0;
		boolean continua = true;
		do {
			System.out.println("Usu�rio, o que deseja fazer? \n"
					+ "1 - Inserir um elemento ao vetor \n"
					+ "2 - Ler o vetor \n"
					+ "3 - Alterar o vetor \n"
					+ "4 - Remover um elemento do vetor \n"
					+ "5 - Sair");
			escolha = sc.nextInt();
			if(escolha < 0 || escolha > 5) 
				System.out.println("Op��o inv�lida");
			else
				continua = false;
		} while(escolha < 0 || escolha > 5 || continua);	
		return escolha;


	}
}
